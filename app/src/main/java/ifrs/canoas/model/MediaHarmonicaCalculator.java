package ifrs.canoas.model;

import java.util.ArrayList;

/**
 * Created by marcio on 06/08/17.
 */

public class MediaHarmonicaCalculator {
    private ArrayList<ItemMedia> notas = new ArrayList();

    public void addNota(String label, double peso, double media){
        notas.add(new ItemMedia(label,peso,media)); //TODO adicionar no arrayList
    }

    public String listaNotas(){
        String stringNotas = null;
        for (ItemMedia nota: notas){
            stringNotas = nota.getNota() + "\n";
        }
        if (stringNotas.equals(null)){
            stringNotas = "Nenhuma nota ainda.";
        }
        return stringNotas;
    }

    public int listaSize(){
        return notas.size();
    }

    /**
     * TODO verificar médias >0 ou 0,0001
     * TODO verificar se a soma dos pesos é igual a 10
     * TODO calcular
     *
     * @return
     * @throws Exception
     */
    public double calcula() throws Exception {
        double media = 0;
        double pesos = 0;
        double somaPeso = 0;
        double notaAux = 0;

        if(notas.size()>2) {
            for (ItemMedia nota : notas) {
                pesos += nota.getPeso();
                notaAux = nota.getNota();
                if (notaAux <= 0) {
                    notaAux = 0.001;
                }
                somaPeso += ((nota.getPeso() / notaAux));
            }
            if (pesos == 10) {
                media = pesos / somaPeso;
            } else {
                System.out.println("Pesos inferiores a 10");
            }
        }else{
            throw new Exception("Quantidade de notas insuficientes");
        }
        return media;
    }
}
