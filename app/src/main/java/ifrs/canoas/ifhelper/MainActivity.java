package ifrs.canoas.ifhelper;

import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;


import java.util.Locale;

import ifrs.canoas.model.MediaHarmonicaCalculator;


//Selecione a aba de todo e resolva todos eles inclusive esse
//TODO transformar essa tela no idioma Inglês e Português
//TODO adicionar um icone para cada Botão.
//TODO adicionar métodos para todas as funções da atividade principal
//TODO Adicionar remoto e submeter a atividade.(ver tutorial da aula).

//Basic activity com floatButton removido
public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }

    /**
     * Evento criado para servir como método de listener.
     *
     * @param v
     */
    public void calculaFaltas(View v) {
        startActivity(new Intent(this, CalculaFaltasActivity.class));
    }

    //TODO add Botão calculadora de notas.
    public void calculaMedias(View v){
        startActivity(new Intent(this, CalculaMediasActivity.class));
    }

    public void setLocale(String lang) {
        Locale myLocale = new Locale(lang);
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);
        Intent refresh = new Intent(this, MainActivity.class);
        refresh.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(refresh);
    }

    public void alterarIdioma(View v){
        Button idioma = (Button) findViewById(R.id.btIdioma);
        if (idioma.getText().toString().equals("Alterar idioma")) {
            setLocale("EN");
        } else {
            setLocale("PT");
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
