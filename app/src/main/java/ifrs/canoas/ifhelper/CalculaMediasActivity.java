package ifrs.canoas.ifhelper;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import ifrs.canoas.model.MediaHarmonicaCalculator;

/**
 * Created by Everton on 12/08/2017.
 */

public class CalculaMediasActivity extends AppCompatActivity {
    private MediaHarmonicaCalculator mediaHarmonica;


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calcula_notas);
        mediaHarmonica = new MediaHarmonicaCalculator();
    }

    public void adicionaNotas(View v){
        int cont = 0;
        EditText valor_Nota = (EditText) findViewById(R.id.numNota);
        EditText pesoNota = (EditText) findViewById(R.id.numPeso);
        EditText numNota = (EditText) findViewById(R.id.numNota);

    }

    public void calculaMedia(View v) throws Exception {
        if (mediaHarmonica.listaSize() > 2) {
            TextView resultado = (TextView) findViewById(R.id.resultado);
            resultado.setText(resultado.getText() + " " + mediaHarmonica.calcula());
        }
    }


}
